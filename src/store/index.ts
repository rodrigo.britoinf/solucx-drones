import Vue from 'vue'
import Vuex from 'vuex'

export const SET_DRONE = `SET_DRONE`;

import { drone } from '@/store/drone'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    // drone: {}
  },
  mutations: {
    // [SET_DRONE] (state, data) {
    //   state.drone = data
    // },
  },
  actions: {
    // [SET_DRONE]({ commit }, data) {
    //   commit(SET_DRONE, data)
    // }
  },
  modules: {
    drone
  }
})
