export const SET_DRONE = `SET_DRONE`;

import Vuex from 'vuex'

export const drone = {
  state: {
    drone: null
  },
  actions: {
    [SET_DRONE]({ commit }: any, data: any) {
      commit(SET_DRONE, data);
    },
  },
  mutations: {

    // COURSES
    [SET_DRONE](state: any, data: any) {
      state.drone = data
    },
  },
};
