import Api from './api';

export default {

    count() {
        return Api.get('').then((response) => {
            return response.data.length
        })
    },

    list(page: number, limit: number, droneId?: string, sort?: string, orderDesc?: boolean) {
        let suffix = ''
        if (droneId) suffix += `/${droneId}`
        if (page && limit) suffix += `/?_page=${page}&_limit=${limit}`
        if (sort) suffix += `&_sort=${sort}`
        orderDesc? suffix += `&_order=desc` : suffix += `&_order=asc`
        return Api.get(suffix).then((response) => {
            if (!response.data.length) return {"data": [response.data]}
            return response
        })
    },

    getById(droneId: number) {
        let suffix = ''
        if (droneId) suffix += `/${droneId}`
        return Api.get(suffix).then((response) => {
            if (!response.data.length) return {"data": [response.data]}
            return response
        })
    }

}